import { UnauthorizedException, UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { SessionAuthGuard } from 'src/auth/session-auth.guard';
import { UpdateUserData } from './dto/update-user.dto';
import { UsersList } from './dto/users-list.dto';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@UseGuards(SessionAuthGuard)
@Resolver(() => User)
export class UsersResolver {
  constructor(private usersService: UsersService) {}

  @Query(() => String)
  sayHello(): string {
    return 'Hello World!';
  }

  @Query(() => User)
  async user(@Args('id', { type: () => String }) userId: string) {
    const user = await this.usersService.findById(userId);
    return user;
  }

  @Mutation(() => User)
  async updateUser(
    @Args({ name: 'updatedData', type: () => UpdateUserData })
    updatedData: Partial<UpdateUserData>,
    @Args({ name: 'userId', type: () => String }) userId: string,
  ) {
    return this.usersService.updateUser(updatedData, userId);
  }

  @Query(() => UsersList)
  async users(
    @Args({ name: 'limit', defaultValue: 10, type: () => Int }) limit: number,
    @Args({ name: 'pagination', defaultValue: 0, type: () => Int })
    pagination: number,
    @Args({ name: 'search', defaultValue: '', type: () => String }) search: string,
  ) {
    const result = await this.usersService.getUsersList(
      limit,
      pagination,
      search,
    );

    return {
      limit: limit,
      pagination: pagination,
      countAll: result.number,
      result: result.users,
    };
  }
}
