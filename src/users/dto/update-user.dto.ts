import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class UpdateUserData {
    @Field({nullable: true})
    email: string;

    @Field({nullable: true})
    password: string;

    @Field({nullable: true})
    phone: string;

    @Field({nullable: true})
    name: string;

    @Field({nullable: true})
    surname: string;
}