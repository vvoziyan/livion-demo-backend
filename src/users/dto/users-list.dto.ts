import { Field, Int, ObjectType, PartialType } from '@nestjs/graphql';
import { User } from '../entities/user.entity';

@ObjectType()
export class UsersList {
  @Field(() => Int, { description: 'Limit' })
  limit: number;
  @Field(() => Int, { description: 'Pagination' })
  pagination: number;
  @Field(() => Int, { description: 'number' })
  countAll: number;
  @Field(() => [User], { description: 'Result' })
  result: User[];
}