export class CreateUserDto {
    email: string;
    password: string;
    phone: string;
    name: string;
    surname: string;
}