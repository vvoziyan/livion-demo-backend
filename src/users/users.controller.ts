import { Body, Controller, Get, Param, ParseIntPipe, Post, Session, SetMetadata, UseGuards } from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';
import { SessionAuthGuard } from 'src/auth/session-auth.guard';
import { GetSession } from 'src/common/user.decotrator';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService, private authsService: AuthService) {}

    @Post('signup')
    async createUser(@Body() createUserDto: CreateUserDto) {
        const newUser = await this.usersService.createUser(createUserDto);
        const session = await this.authsService.login(newUser._id);
        return {token: session.token, userId: session.userId}
    }

    @Get('remove/:id')
    @UseGuards(SessionAuthGuard)
    async removeUser(@Param('id') userId: string) {
        return this.usersService.removeUser(userId);
    }
}

