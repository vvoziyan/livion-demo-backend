import { Field, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UsersDocument = User & Document;

@Schema()
@ObjectType()
export class User {
    @Field()
    _id: string;

    @Prop()
    @Field()
    email: string;
    @Prop()
    passwordHash: string;
    @Prop()
    passwordSalt: string;

    @Prop()
    @Field()
    phone: string;
    @Prop()
    @Field()
    name: string;
    @Prop()
    @Field()
    surname: string;

    @Prop([String])
    @Field(() => [String])
    roles: String[];
}


export const UserSchema = SchemaFactory.createForClass(User);