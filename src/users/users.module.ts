import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { AuthService } from 'src/auth/auth.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Session, SessionSchema } from 'src/auth/session.schema';
import { UsersResolver } from './users.resolver';
import { User, UserSchema } from './entities/user.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema, collection: 'users' },
    ]),
    MongooseModule.forFeature([
      { name: Session.name, schema: SessionSchema, collection: 'sessions' },
    ]),
  ],
  providers: [
    UsersService,
    AuthService,
    UsersResolver
  ],
  exports: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
