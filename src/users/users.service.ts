import {
  ConflictException,
  ForbiddenException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { User, UsersDocument } from './entities/user.entity';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { SessionDocument } from 'src/auth/session.schema';
import { AuthService } from 'src/auth/auth.service';
import { UpdateUserData } from './dto/update-user.dto';
import { randomBytes, pbkdf2Sync } from 'crypto';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UsersDocument>,
    @Inject(forwardRef(() => AuthService)) private authService: AuthService,
  ) {}

  async findByEmail(email: string): Promise<User | undefined> {
    const user = await this.userModel.findOne({ email: email }).exec();
    return user?.toObject();
  }

  async findById(userId: UsersDocument['_id']): Promise<User | undefined> {
    const user = await this.userModel.findById(userId).exec();
    return user?.toObject();
  }
  
  hashPassword(password: string): {salt: string, hash: string} {
    const salt = randomBytes(16).toString('hex');      
    const hash = pbkdf2Sync(password, salt,  
    1000, 64, `sha512`).toString(`hex`); 

    return {salt, hash}
  }

  async createUser(
    createUserDto: CreateUserDto,
  ): Promise<Partial<UsersDocument>> {
    const isUserExists = await this.userModel.exists({
      email: createUserDto.email,
    });

    if (isUserExists) {
      throw new ConflictException(null, "Email already exists");
    }

    const { password, ...userData } = createUserDto;

    const {hash, salt} = this.hashPassword(password);

    const user = await this.userModel.create({
      ...userData,
      passwordHash: hash,
      passwordSalt: salt
    });

    return user.toObject();
  }

  async removeUser(userId: string) {
      await this.authService.clearAllUserSessions(userId);
      await this.userModel.deleteOne({ _id: userId }).exec();
      return;
  }

  async getUsersList(
    limit: number,
    pagination: number,
    search: string,
  ): Promise<{ users: Omit<User, 'passwordHashm' | 'passwordSalt'>[]; number: number }> {
    const regex = new RegExp(`(.*(${search.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')}).*)`, 'i');

    const agregate = [
      {
        $addFields: {
          fullName: {
            $concat: ['$name', ' ', '$surname'],
          },
        },
      },
      {
        $match: { fullName: regex },
      },
    ];

    const users = await this.userModel.aggregate(agregate).skip(pagination).limit(limit).exec();
    const countAll = (await this.userModel.aggregate([...agregate, {$count: "countAll"}]))[0]?.countAll || 0;
    
    return {
      users: users?.map((user) => {
        const { passwordHash, passwordSalt, ...userData } = user;
        return userData;
      }),
      number: countAll
    };
  }

  async updateUser(
    updatedData: Partial<UpdateUserData>,
    userId: string,
  ): Promise<Omit<User, 'passwordHash'>> {
    let isError = false;

    const {password, ...data} = updatedData;

    const {hash, salt} = this.hashPassword(password);

    const updatedUser = await this.userModel
      .findOneAndUpdate({ _id: userId }, {...data, passwordHash: hash, passwordSalt: salt})
      .orFail();

      
    console.log(updatedUser);
    if (updatedUser && !isError) {
      return { ...updatedUser.toObject(), ...updatedData };
    }

    throw new NotFoundException();
  }
}
