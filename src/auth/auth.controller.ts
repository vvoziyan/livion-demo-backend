import {
  Controller,
  Get,
  UseGuards,
  Request,
  UseInterceptors,
  Session,
  Post,
  ExecutionContext,
} from '@nestjs/common';
import { GetSession } from 'src/common/user.decotrator';
import { AuthService } from './auth.service';
import { SessionAuthGuard } from './session-auth.guard';
import { LocalAuthGuard } from './local-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {
    console.log('AuthController created');
  }

  @Post('login')
  @UseGuards(LocalAuthGuard)
  async login(@GetSession() session: any) {
    console.log(session._id.toString(), session.__id)
    console.log(await this.authService.getSessionToken(session._id.toString()))
    return {token: await this.authService.getSessionToken(session._id.toString()), userId: session.userId.toString()};
  }

  @Get('logout')
  @UseGuards(SessionAuthGuard)
  logout(@GetSession() session) {
    console.log(session.id)
    this.authService.logout(session.id)
    return;
  }

  @Get('jwtProtected')
  @UseGuards(SessionAuthGuard)
  async jwtProtected() {
    return '';
  }
}
