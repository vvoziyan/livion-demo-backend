import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UsersDocument } from 'src/users/entities/user.entity';
import { UsersService } from '../users/users.service';
import { Session, SessionDocument } from './session.schema';
import { pbkdf2Sync } from 'crypto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    @InjectModel(Session.name) private sessionModel: Model<SessionDocument>,
    @InjectModel(User.name) private usersModel: Model<UsersDocument>,
  ) {}

  async validateUser(
    email: string,
    password: string,
  ): Promise<Omit<User, 'passwordHash' | 'passwordSalt'>> {
    const user = await this.usersService.findByEmail(email);
    if (user) {
      const hash = pbkdf2Sync(
        password,
        user.passwordSalt,
        1000,
        64,
        `sha512`,
      ).toString(`hex`);
      if (hash === user.passwordHash) {
        const { passwordHash, passwordSalt, ...result } = user;
        return result;
      }
      return null
    }
    return null;
  }

  async login(userId: string): Promise<Session> {
    const now = Date.now();
    const newSession: Session = {
      token:
        (Math.random() * 123456789).toString(36) +
        now.toString(36) +
        (Math.random() * 123456789).toString(36),
      userId: await (await this.usersModel.findById(userId)).toObject()._id,
      timestamp: now,
    };
    const newSessionObject = await this.sessionModel.create(newSession);
    return newSessionObject.toObject();
  }

  async logout(sessionId: string) {
    await this.sessionModel.remove({ id: sessionId });
  }

  async validateSessionToken(jwt: string): Promise<Session> {
    const session = await (
      await this.sessionModel.findOne({ token: jwt })
    )?.toObject();

    if (session) {
      return session;
    }
    return null;
  }

  async getSessionToken(sessionId: string) {
    return (await this.sessionModel.findOne({ _id: sessionId }))?.toObject()
      .token;
  }

  async clearAllUserSessions(userId: string) {
    console.log(userId);
    await this.sessionModel.deleteMany({ userId: userId }).exec();
  }
}
