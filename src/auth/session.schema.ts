import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { User } from 'src/users/entities/user.entity';

export type SessionDocument = Session & Document;

@Schema()
export class Session {
    @Prop({required: true})
    token: string;
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: User.name, required: true })
    userId: User;
    @Prop({required: true})
    timestamp: number
}


export const SessionSchema = SchemaFactory.createForClass(Session);