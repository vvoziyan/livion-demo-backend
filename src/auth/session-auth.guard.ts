import { applyDecorators, CanActivate, ExecutionContext, Inject, Injectable, SetMetadata, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthService } from './auth.service';

@Injectable()
export class SessionAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService
  ) {}

  async canActivate(context: ExecutionContext) {

    const requestContext = context.switchToHttp().getRequest();

    const authorizationHeader: string | undefined = requestContext?.headers?.authorization;
    if (authorizationHeader) {
      const {token, ...session} = await this.validate(authorizationHeader)
      requestContext.userSession= session;
      return true;
    }

    const graphqlContext = GqlExecutionContext.create(context).getContext();
    const graphqlAuthorizationHeader: string | undefined = graphqlContext.req.headers?.authorization;
    if (graphqlAuthorizationHeader) {
      const {token, ...session} = await this.validate(graphqlAuthorizationHeader)
      graphqlContext.userSession = session;
      return true;
    }

    return false;
  }

  async validate(jwt: string): Promise<any> {
    const session = await this.authService.validateSessionToken(jwt);
    if (!session) {
      throw new UnauthorizedException();
    }
    return session;
  }

  private handleRequest(err, user) {
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}